#include "Application2D.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "Player.h"
#include "GameObject.h"

Application2D::Application2D() {

}

Application2D::~Application2D() {

}

bool Application2D::startup() {

	m_2dRenderer = new aie::Renderer2D();

	//Draws the Background into the application
	m_backgroundTexture = new aie::Texture("./textures/snake_background.png");
	
	//Draws font into the scene
	m_font = new aie::Font("./font/consolas.ttf", 32);

	//Camera Position on the scene
	m_cameraX = 0;
	m_cameraY = 0;

	//Creates New Player
	m_player = new Player();

	//Creates New Gameobect (Pick up)
	m_gObject = new GameObject();

	//Timer in the game
	//Runs off Delta Time
	m_timer = 0;
	
	return true;
}

void Application2D::shutdown() {
	
	delete m_player;
	
	delete m_gObject;

	delete m_backgroundTexture;

	delete m_2dRenderer;
}

void Application2D::update(float deltaTime) {

	m_timer += deltaTime;

	// input example
	aie::Input* input = aie::Input::getInstance();

	//Calls arrow keys for movement
	//Refer to Player.h for keybinds
	m_player->Update(deltaTime);

	m_gObject->Update(m_gObject->m_objPosX, m_gObject->m_objPosY, m_gObject->m_objWidth, m_gObject->m_objHeight);

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Application2D::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// set the camera position before we begin rendering
	m_2dRenderer->setCameraPos(m_cameraX, m_cameraY);

	// begin drawing sprites
	m_2dRenderer->begin();

	// Drawing background
	m_2dRenderer->drawSprite(m_backgroundTexture, 0, 0, getWindowWidth(), getWindowHeight(), 0, 0, 0, 0);
	
	//Draws Pick Up
	m_gObject->Draw(m_2dRenderer);

	//Draw Player Spite
	m_player->Draw(m_2dRenderer);

	m_2dRenderer->drawText(m_font, "Score: " + m_gObject->m_score, 30, 720 - 64 );

	// done drawing sprites
	m_2dRenderer->end();
}