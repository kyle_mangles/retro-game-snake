#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "Audio.h"
#include "Player.h"
#include "GameObject.h"


class Application2D : public aie::Application {
public:

	Application2D();
	virtual ~Application2D();

	virtual bool startup();
	virtual void shutdown();


	virtual void update(float deltaTime);
	virtual void draw();

protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Texture*		m_backgroundTexture;
	aie::Font*			m_font;

	float m_playerX, m_playerY;
	float m_cameraX, m_cameraY;
	float m_timer;


	Player* m_player;
	GameObject* m_gObject;
};