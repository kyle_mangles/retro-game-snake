#include "GameObject.h"



GameObject::GameObject()
{
	m_objPosX = 500;
	m_objPosY = 500;
	m_objWidth = 15;
	m_objHeight = 15;

	m_score = 0;

	m_objTexture = new aie::Texture("./textures/PickUp.png");
}


GameObject::~GameObject()
{
	delete m_objTexture;
}

void GameObject::Update(int m_objPosX, int m_objPosY, int m_objWidth, int m_objHeight)
{
	Player m_player;
	if (m_objWidth < m_player.m_playerHeadPosX && m_objPosX > m_player.m_playWidth && m_objHeight < m_player.m_playerHeadPosY && m_objPosY > m_player.m_playHeight)
	{
		m_score + 1;
	}



}

void GameObject::Draw(aie::Renderer2D* Renderer)
{
	Renderer->drawSprite(m_objTexture, m_objPosX, m_objPosY, m_objWidth, m_objHeight);
}