#pragma once

#include "Renderer2D.h"
#include "Texture.h"
#include "Input.h"

class Player
{
public:
	Player();
	~Player();

	void Update(float deltaTime);

	double m_playerHeadPosX;
	double m_playerHeadPosY;

	double m_playerBodyPosX;
	double m_playerBodyPosY;
	
	int m_playWidth;
	int m_playHeight;

	double m_moveSpeed;
	
	double m_playerRot;

	int m_score;
	float m_timer;

	

	void Draw(aie::Renderer2D* Renderer);


private:
	aie::Texture* m_playerHead;

	aie::Texture* m_playerBody;
	
};

