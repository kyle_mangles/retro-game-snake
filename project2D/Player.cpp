#include "Player.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>

Player::Player()
{
	m_playerHeadPosX = 300;
	m_playerHeadPosY = 300;

	m_playerBodyPosX = m_playerHeadPosX;
	m_playerBodyPosY = m_playerHeadPosY;
	
	m_playWidth = 25;
	m_playHeight = 25;

	m_moveSpeed = 200;

	m_playerRot = 0;

	m_playerHead = new aie::Texture("./textures/snake_head.png");

	m_playerBody = new aie::Texture("./textures/snake_body.png");
}

Player::~Player()
{
	delete m_playerHead;

	delete m_playerBody;
}

glm::vec2 MoveDir = { 0,0 };

void Player::Update(float deltaTime)
{
	m_timer += deltaTime;

	// input example
	aie::Input* input = aie::Input::getInstance();

	// use arrow keys to move camera
	if (input->wasKeyPressed(aie::INPUT_KEY_UP))
	{
		MoveDir = { 0,1 };

		//Calculating rotation in radians
		m_playerRot = glm::pi<float>() * 0;
		
		//Stoping the body from moving on the diagonal 
		m_playerBodyPosX = m_playerHeadPosX;

		//Making the body follow the head 
		m_playerBodyPosY = m_playerHeadPosY - 30;
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_DOWN))
	{
		MoveDir = { 0,-1 };

		//Calculating rotation in radians
		m_playerRot = glm::pi<float>() * 1;

		//Stoping the body from moving on the diagonal 
		m_playerBodyPosX = m_playerHeadPosX;

		//Making the body follow the head 
		m_playerBodyPosY = m_playerHeadPosY + 30;

	}

	if (input->wasKeyPressed(aie::INPUT_KEY_LEFT))
	{
		MoveDir = { -1,0 };

		//Calculating rotation in radians
		m_playerRot = glm::pi<float>() * .5;

		//Stoping the body from moving on the diagonal 
		m_playerBodyPosY = m_playerHeadPosY;

		//Making the body follow the head 
		m_playerBodyPosX = m_playerHeadPosX + 30;
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_RIGHT))
	{
		MoveDir = { 1,0 };

		//Calculating rotation in radians
		m_playerRot = glm::pi<float>() * 1.5;
		
		//Stoping the body fro m moving on the diagonal 
		m_playerBodyPosY = m_playerHeadPosY;

		//Making the body follow the head 
		m_playerBodyPosX = m_playerHeadPosX - 30;
	}

	m_playerHeadPosX += MoveDir.x * m_moveSpeed * deltaTime;
	m_playerHeadPosY += MoveDir.y * m_moveSpeed * deltaTime;

	m_playerBodyPosX += MoveDir.x * m_moveSpeed * deltaTime;
	m_playerBodyPosY += MoveDir.y * m_moveSpeed * deltaTime;
}

void Player::Draw(aie::Renderer2D * Renderer)
{
	Renderer->drawSprite(m_playerHead, m_playerHeadPosX, m_playerHeadPosY, m_playWidth, m_playHeight, m_playerRot);
	Renderer->drawSprite(m_playerBody, m_playerBodyPosX, m_playerBodyPosY, m_playWidth, m_playHeight, m_playerRot);
	Renderer->drawSprite(m_playerBody, m_playerBodyPosX , m_playerBodyPosY, m_playWidth	, m_playHeight, m_playerRot);
	//Renderer->drawSprite(m_playerBody, m_playerPosX - 78, m_playerPosY, 25, 25, m_playerRot);
	//Renderer->drawSprite(m_playerBody, m_playerPosX - 104, m_playerPosY, 25, 25, m_playerRot);

}
