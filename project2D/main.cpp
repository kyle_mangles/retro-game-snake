#include "Application2D.h"
#include "main.h"

int main() {
	
	auto app = new Application2D();
	app->run("Snake Retro", 1280, 720, false);
	delete app;

	return 0;
}		