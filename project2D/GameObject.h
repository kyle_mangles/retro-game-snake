#pragma once

#include "Renderer2D.h"
#include "Texture.h"
#include "Player.h"

class GameObject
{
public:
	GameObject();
	~GameObject();

	void Update(int m_objPosX, int m_objPosY, int m_objWidth, int m_objHeight);

	int m_objPosX;
	int m_objPosY;
	int m_objWidth;
	int m_objHeight;

	int m_score;
	void Draw(aie::Renderer2D* Renderer);

private:
	aie::Texture* m_objTexture;

};

